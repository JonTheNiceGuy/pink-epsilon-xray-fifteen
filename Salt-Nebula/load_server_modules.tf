module "Svr1" {
  source = "./Svr1"
  modulename = "Svr1"
  first_three_octets = "198.18.10"
  ami_centos8 = "${var.ami_centos8}"
  ami_ubuntu1804 = "${data.aws_ami.ubuntu.id}"
  ami_win2019 = "${var.ami_win2019}"
  myip = "${trimspace(data.http.icanhazip.body)}/32"
  admin_password = "${var.admin_password}"
  KeyName = "${var.KeyName}"
  salt_master_ip = "${module.nebula_management.salt_master_ip}"
}