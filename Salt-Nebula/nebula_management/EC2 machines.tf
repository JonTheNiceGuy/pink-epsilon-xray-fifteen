variable "ami_centos8" {}
variable "ami_ubuntu1804" {}
variable "ami_win2019" {}
variable "admin_password" {}
variable "KeyName" {}

resource "aws_instance" "iMgmt1_salt" {
  tags = {
    Name = "iMgmt1_salt"
  }

  ami = "${var.ami_ubuntu1804}"
  instance_type = "t2.medium"
  key_name = "${var.KeyName}"
  subnet_id = "${aws_subnet.Subnet.id}"
  vpc_security_group_ids = [
    "${aws_security_group.ServiceSG.id}",
    "${aws_security_group.CommonManagementSG.id}",
    "${aws_security_group.salt_master.id}"
  ]
  associate_public_ip_address = true

  user_data = <<EOF
#! /bin/bash
hostnamectl set-hostname iMgmt1_salt
sed -ri -e "s/127.0.0.1 localhost/127.0.0.1 localhost salt/" /etc/hosts
mkdir /srv
git clone https://jontheniceguy:BDXQ1d-cN_-U6MtqzfWV@gitlab.com/JonTheNiceGuy/pink-epsilon-xray-fourteen /srv/salt
wget -O - https://repo.saltstack.com/py3/ubuntu/18.04/amd64/2019.2/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
echo "deb http://repo.saltstack.com/py3/ubuntu/18.04/amd64/2019.2 bionic main" > /etc/apt/sources.list.d/saltstack.list
apt-get update
bash -c "apt-get install -y salt-master salt-minion"
EOF
}

output "salt_master_ip" {
  value = "${aws_instance.iMgmt1_salt.public_ip}"
}