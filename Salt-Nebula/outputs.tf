output "Resulting_IPs" {
  value = <<EOF

iMgmt1_salt: ${module.nebula_management.salt_master_ip}
iSvr1_mysql: ${module.Svr1.iSvr1_mysql_ip}
iSvr1_win: ${module.Svr1.iSvr1_win_ip}
EOF
}