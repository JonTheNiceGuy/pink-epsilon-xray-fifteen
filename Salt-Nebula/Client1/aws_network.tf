variable modulename {}
variable first_three_octets {}

resource "aws_vpc" "VPC" {
  cidr_block  = "${var.first_three_octets}.0/24"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "n${var.modulename}"
  }
}

resource "aws_subnet" "Subnet" {
  vpc_id     = "${aws_vpc.VPC.id}"
  cidr_block = "${var.first_three_octets}.0/24"
  tags = {
    Name = "s${var.modulename}"
  }
}

resource "aws_internet_gateway" "InternetGateway" {
  vpc_id = "${aws_vpc.VPC.id}"
  tags = {
    Name = "g${var.modulename}"
  }
}

resource "aws_route_table" "RoutingTable" {
  vpc_id = "${aws_vpc.VPC.id}"
  tags = {
    Name = "r${var.modulename}"
  }
}

resource "aws_route" "RoutingTableEntry" {
  route_table_id = "${aws_route_table.RoutingTable.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.InternetGateway.id}"
}

resource "aws_route_table_association" "RoutingTableAssociation" {
  subnet_id = "${aws_subnet.Subnet.id}"
  route_table_id = "${aws_route_table.RoutingTable.id}"
}