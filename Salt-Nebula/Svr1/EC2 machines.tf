variable "ami_centos8" {}
variable "ami_ubuntu1804" {}
variable "ami_win2019" {}
variable "admin_password" {}
variable "KeyName" {}
variable "salt_master_ip" {}
locals {
  ubuntu_client_install = <<EOF
wget -O - https://repo.saltstack.com/py3/ubuntu/18.04/amd64/2019.2/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
echo "deb http://repo.saltstack.com/py3/ubuntu/18.04/amd64/2019.2 bionic main" > /etc/apt/sources.list.d/saltstack.list
apt-get update
bash -c "apt-get install -y salt-minion"
echo "${var.salt_master_ip} salt" >> /etc/hosts
EOF
  windows_client_install = ""
  centos_client_install = ""
}

resource "aws_instance" "iSvr1_mysql" {
  tags = {
    Name = "iSvr1_mysql"
  }

  ami = "${var.ami_ubuntu1804}"
  instance_type = "t2.small"
  key_name = "${var.KeyName}"
  subnet_id = "${aws_subnet.Subnet.id}"
  vpc_security_group_ids = [
    "${aws_security_group.ServiceSG.id}",
    "${aws_security_group.CommonManagementSG.id}"
  ]
  associate_public_ip_address = true

  user_data = <<EOF
#! /bin/bash
hostnamectl set-hostname iSvr1_mysql
apt-get install -y mysql-server
${local.ubuntu_client_install}
EOF
}

resource "aws_instance" "iSvr1_win" {
  tags = {
    Name = "iSvr1_win"
  }

  ami = "${var.ami_win2019}"
  instance_type = "t2.medium"
  key_name = "${var.KeyName}"
  subnet_id = "${aws_subnet.Subnet.id}"
  vpc_security_group_ids = [
    "${aws_security_group.ServiceSG.id}",
    "${aws_security_group.CommonManagementSG.id}"
  ]
  associate_public_ip_address = true

  user_data = <<EOF
<powershell>
  netsh advfirewall firewall add rule name="WinRM in" protocol=TCP dir=in profile=any localport=5985 remoteip=any localip=any action=allow
  # Set Administrator password
  $admin = [adsi]("WinNT://./administrator, user")
  $admin.psbase.invoke("SetPassword", "${var.admin_password}")
</powershell>
<script>
  echo Invoke-WebRequest -Uri 'https://repo.saltstack.com/windows/Salt-Minion-2019.2.2-Py3-AMD64-Setup.exe' -OutFile "$env:SystemRoot\Temp\Salt-Minion-Setup.exe" > "%SystemRoot%\Temp\InstallSalt.ps1"
  echo cmd /c %SystemRoot%\Temp\Salt-Minion-Setup.exe /S /master=${var.salt_master_ip} /minion-name=iSvr1_win >> "%SystemRoot%\Temp\InstallSalt.ps1"
  cmd /c powershell -NoProfile -ExecutionPolicy Bypass -File "%SystemRoot%\Temp\InstallSalt.ps1"
</script>
EOF
}

output "iSvr1_mysql_ip" {
  value = "${aws_instance.iSvr1_mysql.public_ip}"
}

output "iSvr1_win_ip" {
  value = "${aws_instance.iSvr1_win.public_ip}"
}