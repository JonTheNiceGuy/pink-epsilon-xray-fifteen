# Overlay Networks Research
## Why look at Overlay Networks?

Overlay networks provide a way of "layering" a second, virtual, IP network over the top of your existing physical IP network. A good, and regularly used example of this is in a VPN network for remote or home workers, where they receive a "second" IP address on the VPN interface that allows them access to their work resources. People who don't have access to this second network can't access the resources on the work network.

These VPNs typically use IPsec tunnel, [AnyConnect](http://www.cisco.com/go/asm)/[OpenConnect](https://tools.ietf.org/html/draft-mavrogiannopoulos-openconnect-02)/[Pulse Connect Secure](https://www.pulsesecure.net/products/connect-secure/)/[Palo Alto GlobalProtect SSL VPN](https://www.paloaltonetworks.com/features/vpn), [Microsoft SSTP](https://msdn.microsoft.com/en-us/library/cc247338.aspx) or other VPN products.

A [recent announcement](https://slack.engineering/introducing-nebula-the-open-source-global-overlay-network-from-slack-884110a5579) from Slack about their "[Nebula](https://github.com/slackhq/nebula)" product, and the first response to that post referencing [ZeroTier](https://www.zerotier.com/)'s product [ZeroTierOne](https://github.com/zerotier/ZeroTierOne) triggered some interest in me, in particular because these specific overlay networks provide secured access to resources. Nebula and ZeroTier have clients for all major Desktop and Server OS platforms. Nebula also has an iOS client in the works, while ZeroTier has both Android and iOS clients available today. Nebula and ZeroTier also have in-built packet filtering capabilities.

Docker Swarm has an overlay network product too, but I'm not sure

## What is in here?

This repo will provide the infrastructure to build and test several overlay products, and allow us to test multiple systems using a simple methodology.

1. How easy is any server or management portion to set up?
2. How easy is any client portion to set up?
3. How quickly can management changes be pushed to clients?
4. How performant is the VPN once it's established?

## What will this provision?

1. A management Virtual Network, nMgmt1 (198.18.1.0/24) with *all required* management servers.
2. Two server Virtual Networks, nSvr1 (198.18.10.0/24) and nSvr2 (198.18.20.0/24)
3. Two client Virtual Networks, nClient1 (198.18.30.0/24) and nClient2 (198.18.40.0/24)
4. A collection of trusted machines:
   1. A Microsoft Windows server running Active Directory, iSvr1_ad (198.18.10.10)
   2. An Ubuntu 18.04 LTS Server, running the MySQL database server iSvr1_mysql (198.18.10.20)
   3. A CentOS 7 web server, iSvr2_web (198.18.20.10)
   4. A Microsoft Windows server as a client machine, iClient1_win (198.18.30.10)
   5. An Ubuntu 18.04 LTS server as a client machine, iClient2_lin (198.18.40.10)
5. A collection of untrusted machines:
   1. An Ubuntu 18.04 LTS Server iSvr1_untrust (198.18.10.30)
   2. A Microsoft Windows server iClient2_win (198.18.40.20)

