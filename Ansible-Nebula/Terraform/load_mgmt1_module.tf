module "Mgmt1" {
  source = "./Mgmt1"
  modulename = "Mgmt1"
  first_three_octets = "198.18.1"
  ami_centos8 = "${var.ami_centos8}"
  ami_ubuntu1804 = "${data.aws_ami.ubuntu.id}"
  ami_win2019 = "${var.ami_win2019}"
  myip = "${trimspace(data.http.icanhazip.body)}/32"
  admin_password = "${var.admin_password}"
  KeyName = "${var.KeyName}"
}