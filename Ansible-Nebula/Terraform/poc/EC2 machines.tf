variable "ami_centos8" {}
variable "ami_ubuntu1804" {}
variable "ami_win2019" {}
variable "admin_password" {}
variable "KeyName" {}

resource "aws_instance" "instance_a" {
  tags = {
    Name = "i${var.modulename}_a"
  }

  ami = "${var.ami_ubuntu1804}"
  instance_type = "t2.medium"
  key_name = "${var.KeyName}"
  subnet_id = "${aws_subnet.Subnet.id}"
  vpc_security_group_ids = [
    "${aws_security_group.ServiceSG.id}",
    "${aws_security_group.CommonManagementSG.id}"
  ]
  associate_public_ip_address = true

  user_data = <<EOF
#! /bin/bash
hostnamectl set-hostname i${var.modulename}_a
EOF
}

# resource "aws_instance" "instance_b" {
#   tags = {
#     Name = "i${var.modulename}_b"
#   }

#   ami = "${var.ami_ubuntu1804}"
#   instance_type = "t2.medium"
#   key_name = "${var.KeyName}"
#   subnet_id = "${aws_subnet.Subnet.id}"
#   vpc_security_group_ids = [
#     "${aws_security_group.ServiceSG.id}",
#     "${aws_security_group.CommonManagementSG.id}"
#   ]
#   associate_public_ip_address = true

#   user_data = <<EOF
# #! /bin/bash
# hostnamectl set-hostname i${var.modulename}_b
# EOF
# }

output "ips" {
  value = <<EOF
i${var.modulename}_a: ${aws_instance.instance_a.public_ip}
EOF
}