variable modulename {}
variable first_three_octets {}
variable "myip" {}

resource "aws_vpc" "VPC" {
  cidr_block  = "${var.first_three_octets}.0/24"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "n${var.modulename}"
  }
}

resource "aws_subnet" "Subnet" {
  vpc_id     = "${aws_vpc.VPC.id}"
  cidr_block = "${var.first_three_octets}.0/24"
  tags = {
    Name = "s${var.modulename}"
  }
}

resource "aws_internet_gateway" "InternetGateway" {
  vpc_id = "${aws_vpc.VPC.id}"
  tags = {
    Name = "g${var.modulename}"
  }
}

resource "aws_route_table" "RoutingTable" {
  vpc_id = "${aws_vpc.VPC.id}"
  tags = {
    Name = "r${var.modulename}"
  }
}

resource "aws_route" "RoutingTableEntry" {
  route_table_id = "${aws_route_table.RoutingTable.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.InternetGateway.id}"
}

resource "aws_route_table_association" "RoutingTableAssociation" {
  subnet_id = "${aws_subnet.Subnet.id}"
  route_table_id = "${aws_route_table.RoutingTable.id}"
}

resource "aws_security_group" "ServiceSG" {
  name = "ServiceSG"
  description = "Service security group"
  vpc_id = "${aws_vpc.VPC.id}"

  tags = {
    Name = "ServiceSG"
  }

  ingress {
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol = "tcp"
    from_port = 443
    to_port = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "CommonManagementSG" {
  name = "CommonManagementSG"
  description = "Common Management security group"
  vpc_id = "${aws_vpc.VPC.id}"

  tags = {
    Name = "CommonManagementSG"
  }

  ingress {
    protocol = "tcp"
    from_port = 22
    to_port = 22
    cidr_blocks = ["${var.myip}"]
  }

  ingress {
    protocol = "tcp"
    from_port = 3389
    to_port = 3389
    cidr_blocks = ["${var.myip}"]
  }

  ingress {
    protocol = "icmp"
    from_port = -1
    to_port = -1
    cidr_blocks = ["${var.myip}"]
  }
}
