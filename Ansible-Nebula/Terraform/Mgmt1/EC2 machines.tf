variable "ami_centos8" {}
variable "ami_ubuntu1804" {}
variable "ami_win2019" {}
variable "admin_password" {}
variable "KeyName" {}

resource "aws_instance" "iMgmt1_awx" {
  tags = {
    Name = "iMgmt1_awx"
  }

  ami = "${var.ami_ubuntu1804}"
  instance_type = "t2.medium"
  key_name = "${var.KeyName}"
  subnet_id = "${aws_subnet.Subnet.id}"
  vpc_security_group_ids = [
    "${aws_security_group.ServiceSG.id}",
    "${aws_security_group.CommonManagementSG.id}"
  ]
  associate_public_ip_address = true

  user_data = <<USERDATA
#! /bin/bash
hostnamectl set-hostname iMgmt1_awx
apt-get update
apt-get install -y python3-pip python-pip
pip3 install ansible ansible-tower-cli
pip2 install docker docker-compose
git clone https://gist.github.com/deac8432fea5d39184ced1f9f745679a.git /tmp/build_playbook
cd /tmp/build_playbook
export AWX_Hostname=awx.$(ec2metadata --public-ipv4).nip.io
export AWX_Password="${var.admin_password}"
ansible-playbook install.yml -e "{'ansible_fqdn':'$AWX_Hostname','admin_password':'$AWX_Password'}"
export EXTRA_VARS="{\"ansible_fqdn\":\"$AWX_Hostname\",\"admin_password\":\"$AWX_Password\",\"users\":{\"jon\":{}},\"organizations\":{\"Nebula\":{\"description\": \"Managing Nebula Overlay\",\"teams\": {\"admins\":{\"roles\":{\"admin\":[\"jon\"]}}}}}}"
echo "$EXTRA_VARS" | jq .
ansible-playbook configure_users.yml -e "$EXTRA_VARS"
USERDATA

  # Based on https://stackoverflow.com/a/12748070
  # and notes from https://github.com/hashicorp/terraform/issues/4668
  # -w '%%' syntax from https://github.com/terraform-providers/terraform-provider-template/issues/50
  provisioner "local-exec" {
    command = "until [ $(curl -s -w '%%{http_code}' https://awx.${aws_instance.iMgmt1_awx.public_ip}.nip.io/api/ -o /dev/null) -eq 200 ] ; do curl -s -w 'Response from %%{url_effective} was %%{http_code}\n' https://awx.${aws_instance.iMgmt1_awx.public_ip}.nip.io/api/ -o /dev/null ; sleep 5 ; done"
  }
}

output "ips" {
  value = <<EOF
iMgmt1_a: ${aws_instance.iMgmt1_awx.public_ip}
EOF
}